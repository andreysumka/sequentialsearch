package ru.sag.sort;

import java.util.Scanner;

public class BinarySearch {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int item, high;
        int low = 0;
        int[] array = {-7, -5, 0, 5, 9, 12, 18, 23, 35, 48, 56, 73, 89, 97, 104};

        System.out.println("Введите элемент для поиска: ");
        item = sc.nextInt();
        high = array.length - 1;

        int index = binarySearch(array, low, high, item);
        if(index != -1){
            System.out.println(item + " находится под индексом " + binarySearch(array, low, high, item));
        }
        if(index == -1){
            System.out.println("Такого элемента в массиве нет!");
        }

    }

    public static int binarySearch(int[] array, int low, int high, int item) {
        int mid;

        mid = (low + high) / 2;

        while ((array[mid] != item) && (low <= high)) {
            if (array[mid] > item) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
            mid = (low + high) / 2;
        }
        if (low <= high) {
            return mid;
        }
        return -1;
    }
}
