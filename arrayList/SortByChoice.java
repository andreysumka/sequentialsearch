package ru.sag.sort.arrayList;

import java.util.ArrayList;
import java.util.Arrays;

public class SortByChoice {
    public static void main(String[] args) {

        ArrayList<Integer> array = new ArrayList<>();

        array.add(3);
        array.add(6);
        array.add(8);
        array.add(1);
        array.add(5);
        array.add(9);
        array.add(2);
        array.add(4);
        array.add(7);
        array.add(10);

        selectionSort(array);
    }
    public static void selectionSort(ArrayList<Integer> array) {
        for (int number = 0; number < array.size(); number++) {
            int min = array.get(number);
            int minNum = number;
            for (int num = number + 1; num < array.size(); num++) {
                if (array.get(num) < min) {
                    min = array.get(num);
                    minNum = num;
                }
            }

            int replace = array.get(number);
            array.set(number, array.get(minNum));
            array.set(minNum, replace);
        }
        System.out.println(Arrays.toString(new ArrayList[]{array}));
    }
}