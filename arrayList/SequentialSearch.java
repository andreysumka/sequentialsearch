package ru.sag.sort.arrayList;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Последовательный поиск
 *
 * @author Sumka Andrey 18it18
 */

public class SequentialSearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> arrayList = new ArrayList<>();
        System.out.println(random(arrayList));
        System.out.println("Введите число, которое хотите найти: ");
        int number = scanner.nextInt();
        System.out.println(search(arrayList, number) + 1);
    }

    /**
     * Метод, создающий рандомные значения, записывая их в массив
     *
     * @param arrayList массив значений
     */
    private static ArrayList<Integer> random(ArrayList<Integer> arrayList) {
        int number = (int) (Math.random() * 15) + 10;
        for (int i = 0; i < number; i++) {
            arrayList.add((int) (Math.random() * 100));
        }
        return arrayList;
    }

    /**
     * Метод последовательного поиска
     *
     * @param arrayList массив, в котором хранятся значения
     * @param number число, которое ищет пользователь
     * @return возвращаем индекс числа
     */
    private static int search(ArrayList<Integer> arrayList, int number) {
        int index = -1;
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i) == number) {
                index = i;
            }
        }
        return index;
    }
}
